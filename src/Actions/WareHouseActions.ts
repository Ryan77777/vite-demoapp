import { Dispatch } from "redux";
import {
    SEARCH_WAREHOUSE_FAILURE,
    SEARCH_WAREHOUSE_REQUEST,
    SEARCH_WAREHOUSE_SUCCESS,
} from "../Constants";
import { DummyData } from "../Models";
import { IRootState } from "../Stores";

// Action Creators
export const searchWarehouse =
    (phone_number: string) =>
    (dispatch: Dispatch, getState: () => IRootState) => {
        dispatch(searchWarehouseRequest());

        const { warehouse } = getState().warehouse;

        // Simulate API request delay
        setTimeout(() => {
            const warehouseData = warehouse.filter(
                (data: DummyData) => data.phone_number === phone_number
            );

            if (warehouseData.length > 0) {
                dispatch(searchWarehouseSuccess(warehouseData[0]));
            } else {
                dispatch(searchWarehouseFailure("Data not found"));
            }
        }, 5000);
    };

export const searchWarehouseRequest = () => ({
    type: SEARCH_WAREHOUSE_REQUEST,
});

export const searchWarehouseSuccess = (data: DummyData) => ({
    type: SEARCH_WAREHOUSE_SUCCESS,
    payload: data,
});

export const searchWarehouseFailure = (error: string) => ({
    type: SEARCH_WAREHOUSE_FAILURE,
    payload: error,
});
