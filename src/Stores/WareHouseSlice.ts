import { createSlice } from "@reduxjs/toolkit";
import {
    SEARCH_WAREHOUSE_FAILURE,
    SEARCH_WAREHOUSE_REQUEST,
    SEARCH_WAREHOUSE_SUCCESS,
} from "../Constants";
import { Data } from "../Data";
import { WarehouseState } from "../Models";

const initialState: WarehouseState = {
    warehouse: Data,
    result: null,
    isLoading: false,
};

const WareHouseSlice = createSlice({
    name: "warehouse",
    initialState,
    reducers: {
        resetResult: (state) => {
            state.result = null;
        },
    },
    extraReducers: (builder) => {
        builder.addCase(SEARCH_WAREHOUSE_REQUEST, (state) => {
            state.isLoading = true;
        });

        builder.addCase(SEARCH_WAREHOUSE_SUCCESS, (state, action: any) => {
            state.isLoading = false;
            state.result = action.payload;
        });

        builder.addCase(SEARCH_WAREHOUSE_FAILURE, (state) => {
            state.isLoading = false;
            state.result = null;
        });
    },
});

export const { resetResult } = WareHouseSlice.actions;
export default WareHouseSlice.reducer;
