import React from "react";
import { HeaderProps } from "../Models";

const Header: React.FC<HeaderProps> = ({ title }) => {
    return (
        <header className="bg-white p-4 shadow-md grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
            <h1 className="text-xl font-semibold text-black col-span-full md:col-span-2 lg:col-span-3">
                {title}
            </h1>
        </header>
    );
};

export default React.memo(Header);
