import L from "leaflet";
import React, { CSSProperties } from "react";
import {
    Circle,
    MapContainer,
    Marker,
    Popup,
    TileLayer,
    useMap,
} from "react-leaflet";
import { useAppSelector } from "../Hooks";
import { MapProps } from "../Models";
import { IRootState } from "../Stores";
import Form from "./Form";
import { calculateAge } from "../Utils";

const Map: React.FC<MapProps> = ({ token }) => {
    const { result } = useAppSelector((state: IRootState) => state.warehouse);

    const mapStyle: CSSProperties = {
        width: "100%",
        height: `${window.innerHeight - 60}px`,
        position: "relative",
    };

    const center: [number, number] = [-2.5489, 118.0149];

    const MarkerIcon = L.icon({
        iconUrl:
            "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon.png",
        iconRetinaUrl:
            "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon-2x.png",
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
    });

    const FlyToMarker = () => {
        const map = useMap();
        if (result) {
            map.flyTo([result.lat, result.long], 12);
        }
        return null;
    };

    return (
        <MapContainer
            center={center}
            zoom={5}
            style={mapStyle}
            dragging={true}
            touchZoom={false}
            doubleClickZoom={false}
            scrollWheelZoom={false}
            zoomControl={false}
        >
            <TileLayer
                attribution="&copy; <a href='https://www.mapbox.com/'>Mapbox</a>"
                url={`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${token}`}
                id="mapbox/streets-v11"
            />
            {result && (
                <div>
                    <FlyToMarker />
                    <Marker
                        key={result?.national_id}
                        position={[result?.lat, result?.long]}
                        icon={MarkerIcon}
                    >
                        <Popup>
                            <div className="flex flex-col w-[250px] items-center">
                                <img
                                    src={result?.photo}
                                    alt="Warehouse"
                                    className="w-20 h-20 object-cover rounded-lg mb-2"
                                />
                                <h2 className="text-xl font-bold mb-1">
                                    {result?.full_name}
                                </h2>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        National ID:
                                    </span>
                                    <span className="text-xs text-slate-500">
                                        {result?.national_id}
                                    </span>
                                </div>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        DTM:
                                    </span>
                                    <span className="text-xs text-slate-500">
                                        {result?.dtm}
                                    </span>
                                </div>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        MSISDN:
                                    </span>
                                    <span className="text-xs text-slate-500">
                                        {result?.phone_number}
                                    </span>
                                </div>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        IMSI:
                                    </span>
                                    <span className="text-xs text-slate-500">
                                        {result?.imsi}
                                    </span>
                                </div>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        IMEI:
                                    </span>
                                    <span className="text-xs text-slate-500">
                                        {result?.imei}
                                    </span>
                                </div>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        Age:
                                    </span>
                                    <span className="text-xs text-slate-500">
                                        {calculateAge(result?.date_of_birth)}
                                    </span>
                                </div>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        Address:
                                    </span>
                                    <a
                                        href={`https://www.google.com/maps/search/?api=1&query=${result?.lat},${result?.long}`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                        className="text-xs hover:underline"
                                    >
                                        {result?.address}
                                    </a>
                                </div>
                                <div className="flex flex-row w-full items-center justify-between">
                                    <span className="text-xs font-bold">
                                        Social Media:
                                    </span>
                                    <span className="text-xs text-slate-500">
                                        {result?.social_media ?? "-"}
                                    </span>
                                </div>
                            </div>
                        </Popup>
                    </Marker>
                    <Circle
                        center={[result?.lat, result?.long]}
                        radius={5000}
                        pathOptions={{ color: "red" }}
                    />
                </div>
            )}
            <Form />
        </MapContainer>
    );
};

export default React.memo(Map);
