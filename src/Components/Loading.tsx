import Lottie from "lottie-react";
import React from "react";
import Searching from "../Assets/Animation/Searching.json";
import { useAppSelector } from "../Hooks";
import { IRootState } from "../Stores";

const Loading = () => {
    const { isLoading } = useAppSelector(
        (state: IRootState) => state.warehouse
    );

    return (
        <div
            className={`fixed top-0 left-0 right-0 bottom-0 z-[999] bg-gray-900 bg-opacity-50 flex items-center justify-center ${
                isLoading ? "block" : "hidden"
            }`}
        >
            <div className="w-full h-full flex items-center justify-center">
                <Lottie
                    animationData={Searching}
                    className="max-w-full max-h-full"
                />
            </div>
        </div>
    );
};

export default React.memo(Loading);
