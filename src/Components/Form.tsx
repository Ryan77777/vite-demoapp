import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { FormData } from "../Models";
import { useAppDispatch } from "../Hooks";
import { searchWarehouse } from "../Actions/WareHouseActions";

const Form: React.FC = () => {
    const dispatch = useAppDispatch();

    const { register, handleSubmit, setValue } = useForm<FormData>();

    const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue("phoneNumber", e.target.value);
    };

    const handleSearchSubmit: SubmitHandler<FormData> = (data) => {
        dispatch(searchWarehouse(data.phoneNumber));
    };

    return (
        <form
            className="absolute top-4 left-4 z-[999] bg-white p-4 py-2 rounded-md"
            onSubmit={handleSubmit(handleSearchSubmit)}
        >
            <input
                type="text"
                {...register("phoneNumber")}
                placeholder="Masukkan No Telepon..."
                className="p-2 border border-gray-300 rounded m-2"
                onChange={handleSearchChange}
            />
            <button
                type="submit"
                className="px-4 py-2 bg-blue-500 text-white rounded"
            >
                Cari
            </button>
        </form>
    );
};

export default React.memo(Form);
