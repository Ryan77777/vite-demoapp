import { useAppSelector } from "../Hooks";
import { IRootState } from "../Stores";

const Details = () => {
    const { result } = useAppSelector((state: IRootState) => state.warehouse);

    const data = [
        { label: "NIK", value: result?.national_id },
        { label: "Provinsi", value: result?.province },
        { label: "Zodiak", value: result?.zodiac },
        { label: "NKK", value: result?.family_id },
        { label: "Kota", value: result?.city },
        { label: "Shio", value: result?.shio },
        { label: "Nama", value: result?.full_name },
        { label: "Tipe Kota", value: "Kabupaten" },
        { label: "Hijriyah", value: result?.hijriyah },
        { label: "Jenis Kelamin", value: result?.gender },
        { label: "Kecamatan", value: result?.district },
        { label: "Tahun Ganjil Genap", value: result?.oddEvenYear },
        { label: "Tanggal Lahir", value: result?.date_of_birth },
        { label: "Alamat", value: result?.address },
        { label: "Status", value: "AKTIF" },
        { label: "Tempat Lahir", value: result?.place_of_birth },
        { label: "RW", value: result?.rw },
        { label: "Last Update", value: "2013-10-10 09:20:23" },
        { label: "Kawin", value: result?.marital_status },
        { label: "RT", value: result?.rt },
    ];

    return (
        result && (
            <div className="m-4">
                <h2 className="text-xl font-bold mb-4">Data E-KTP</h2>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-x-4">
                    {data.map((item, index) => (
                        <div
                            key={index}
                            className="flex bg-gray-200 p-4 shadow-md border border-gray-300"
                        >
                            <div className="w-1/2 pr-2 border-r border-gray-300">
                                <span className="font-bold text-gray-700">
                                    {item.label}
                                </span>
                            </div>
                            <div className="w-1/2 pl-2">
                                <span className="text-gray-600">
                                    {item.value}
                                </span>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    );
};

export default Details;
