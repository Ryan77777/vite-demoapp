export interface FormData {
    phoneNumber: string;
}

export interface MapProps {
    token: string;
}

export interface HeaderProps {
    title: string;
}

export interface DummyData {
    full_name: string;
    national_id: string;
    place_of_birth: string;
    date_of_birth: string;
    gender: string;
    address: string;
    religion: string;
    marital_status: string;
    occupation: string;
    citizenship: string;
    blood_type: string;
    phone_number: string;
    photo: string;
    lat: number;
    long: number;
    dtm: string;
    imsi: number;
    imei: number;
    bts: string;
    social_media: string;
    province: string;
    zodiac: string;
    family_id: string;
    city: string;
    shio: string;
    hijriyah: string;
    district: string;
    oddEvenYear: string;
    rw: number;
    rt: number;
}

export interface WarehouseState {
    warehouse: DummyData[];
    result: DummyData | null;
    isLoading: boolean;
}
