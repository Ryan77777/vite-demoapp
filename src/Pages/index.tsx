import { useEffect } from "react";
import Details from "../Components/Details";
import Header from "../Components/Header";
import Loading from "../Components/Loading";
import Map from "../Components/Map";
import { useAppDispatch } from "../Hooks";
import { resetResult } from "../Stores/WareHouseSlice";

const App: React.FC = () => {
    const dispatch = useAppDispatch();
    const token = import.meta.env.VITE_MAPBOX_TOKEN;

    useEffect(() => {
        dispatch(resetResult());
    }, []);

    return (
        <>
            <Loading />
            <Header title="Cek Lokasi" />
            <Map token={token} />
            <Details />
        </>
    );
};

export default App;
